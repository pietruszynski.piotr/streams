package pl.edu.pw.ee;

import lombok.NonNull;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Server implements CommandLineRunner {

    private final Stream stream;

    public Server(@NonNull Stream stream) {
        this.stream = stream;
    }

    public static void main(String[] args) {
        SpringApplication.run(Server.class);
    }

    @Override
    public void run(String... args) {
        // max word fibonacci in long
        // long number = 93;
        // max words sum fibonacci in long
        long number = 31;
        stream.runSequence(number);
        stream.runParallel(number);
    }
}
