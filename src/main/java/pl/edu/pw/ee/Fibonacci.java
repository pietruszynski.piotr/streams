package pl.edu.pw.ee;

public class Fibonacci {

    public static long calculate(long number) {
        if (number >= 0 && number <= 1) {
            return number;
        } else {
            long f0 = 0;
            long f1 = 1;
            long temp = 0;
            for (int i = 2; i <= number; i++) {
                temp = f0 + f1;
                f0 = f1;
                f1 = temp;
            }
            return temp;
        }
    }

}
