package pl.edu.pw.ee;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.LongStream;

@Component
@Slf4j
public class Stream {

    public void runSequence(long number) {
        Monitor monitor = MonitorFactory.start("Sequence");
        long[] longs = calculateSequenceFibonacciTo(number);
        long[] filtered = filterEvenSequence(longs);
        long sum = sumSequence(filtered);
        log.info("Fibonacci sequence even sum is: " + sum);
        monitor.stop();
        log.info("Execute sequence time is: " + monitor.getTotal());
    }

    public void runParallel(long number) {
        Monitor monitor = MonitorFactory.start("Parallel");
        long[] longs = calculateParallelFibonacciTo(number);
        long[] filtered = filterEvenParallel(longs);
        long sum = sumParallel(filtered);
        log.info("Fibonacci parallel even sum is: " + sum);
        monitor.stop();
        log.info("Execute parallel time is: " + monitor.getTotal());

    }

    public long sumSequence(long[] longs) {
        return Arrays.stream(longs)
                .sum();
    }

    public long sumParallel(long[] longs) {
        return Arrays.stream(longs)
                .parallel()
                .sum();
    }

    public long[] filterEvenSequence(long[] longs) {
        return Arrays.stream(longs)
                .filter(l -> l % 2 == 0)
                .toArray();
    }

    public long[] filterEvenParallel(long[] longs) {
        return Arrays.stream(longs)
                .parallel()
                .filter(l -> l % 2 == 0)
                .toArray();
    }

    public long[] calculateSequenceFibonacciTo(long number) {
        return LongStream.range(0, number)
                .map(Fibonacci::calculate)
                .toArray();
    }

    public long[] calculateParallelFibonacciTo(long number) {
        return LongStream.range(0, number)
                .parallel()
                .map(Fibonacci::calculate)
                .toArray();
    }

}
